# Installation Instructions
In order to do the programming exercises of the course, 
you need to have the following software installed:

* [Node JS](https://nodejs.org/en/download/)
* [Microsoft Visual Code](https://code.visualstudio.com/) or
    * [JetBrains WebStorm (_30 days trial_)](https://www.jetbrains.com/webstorm/download)
* [Google Chrome](https://www.google.se/chrome/browser/desktop/) or
    * Firefox or MS Edge
* [GIT Client](https://git-scm.com/downloads)

During the course we will install additional software, such as the TypesScript compiler (`tsc`) and TS Node runner (`ts-node`), using NPM.

