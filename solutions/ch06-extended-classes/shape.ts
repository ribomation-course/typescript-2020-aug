
export default abstract class Shape {
    abstract area(): number;
    toString(): string {
        return `${this.constructor.name}: area=${this.area()}`;
    }
}
