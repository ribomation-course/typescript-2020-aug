import Shape from "./shape";

export default class Rect extends Shape {
    constructor(
        private _width:number,
        private _height:number
    ) {  
        super();
     }

    area(): number {return this._width * this._height;}
    get width():number {return this._width;}
    get height():number {return this._height;}
}
