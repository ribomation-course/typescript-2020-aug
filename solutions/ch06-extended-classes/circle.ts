import Shape from "./shape";

export default class Circle extends Shape {
    constructor(
        private _radius:number
    ) {  
        super();
     }

    area(): number {return this._radius * this._radius * Math.PI;}
    get radius():number {return this._radius;}
}

