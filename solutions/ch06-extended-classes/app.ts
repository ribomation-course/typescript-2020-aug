import {argv} from 'process';
import Rect from "./rect";
import Circle from "./circle";
import Shape from "./shape";
import Triangle from './triangle';

const r = new Rect(10, 5);
//console.log('rect(%d,%d): area=%d', r.width, r.height, r.area())

const c = new Circle(10)
//console.log('circ(%d): area=%d', c.radius, c.area())

function createShape(): Shape {
    const uniform = (lb, ub) => Math.floor(lb + (ub - lb + 1) * Math.random());
    switch(uniform(1,3)) {
        case 1: return new Rect(uniform(2,8), uniform(2,8));
        case 2: return new Circle(uniform(1,10));
        case 3: return new Triangle(uniform(2,8), uniform(2,8));
    }
    throw new Error('unexpected')
}

const N:number = +argv[2] || 10;
const shapes = Array(N).fill(0).map(_ => createShape())
shapes.forEach(s => console.log(s.toString()))
