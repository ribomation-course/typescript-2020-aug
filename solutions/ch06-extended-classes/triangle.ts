import Shape from "./shape";

export default class Triangle extends Shape {
    constructor(
        private _base:number,
        private _height:number
    ) {  
        super();
     }

    area(): number {return this._base * this._height / 2;}
    get base():number {return this._base;}
    get height():number {return this._height;}
}

