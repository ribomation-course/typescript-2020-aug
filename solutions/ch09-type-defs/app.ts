import {snakeCase} from 'lodash';

const input = 'foo Bar--FEE';
const result = snakeCase(input);
console.log('"%s" --> "%s"', input, result);

