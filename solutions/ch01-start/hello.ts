import * as process from 'process'

const name:string  = process.argv[2] || 'Nisse'
const value:number = parseInt(process.argv[3]) || 42

console.log('name: %s, value:%d ==> %d',
     name, value, 2*value)

let obj = {name,value}
console.log(obj)
