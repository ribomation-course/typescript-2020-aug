
function transform(arr: any[], f: (x:any) => any) : any[] {
    for (let k=0; k<arr.length; ++k) {
        arr[k] = f( arr[k] );
    }
    return arr;
}

console.log(transform([1,2,3,4], n => n*n));
console.log(transform(['one', 'two', 'three'], n => n.toUpperCase()));
console.log(transform([false,true,false,true], b => !b));
console.log([false,true,false,true].map(b => !b))
