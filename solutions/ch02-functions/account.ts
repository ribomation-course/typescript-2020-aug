type AccountType = 'check' | 'save' | 'invest';

function createAccount(
    type: AccountType = 'check',
    owner: string = 'nobody',
    balance:number = 0,
    rate:number = 1.5,
    overdrawable: boolean = true
): object {
    return {type, owner, balance, rate, overdrawable}
}

console.log(createAccount())

