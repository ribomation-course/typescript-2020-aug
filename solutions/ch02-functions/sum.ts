import * as process from 'process';

function sum(n: number): number {
    return n * (n + 1) / 2;
}

const n: number = +(process.argv[2]) || 10;
console.log('sum(%d) = %d', n, sum(n));
