import { readFileSync } from 'fs';
import axios from 'axios';
import { User, Address, Company } from './user';

//const dataFile = './users.json';
//const txt = readFileSync(dataFile).toString();
//const data: User[] = JSON.parse(txt);

async function loadUsers(): Promise<User[]> {
    const url = 'http://jsonplaceholder.typicode.com/users';
    try {
        const response = await axios.get(url);
        return response.data;
    } catch (err) {
        console.error('Failed: %s', err.message);
        return [];
    }
}

(async () => {
    const data = await loadUsers();
    console.log('loaded %d users', data.length);
    data
        .filter(u => u.email.endsWith('.biz'))
        .map(u => u.name)
        .forEach(n => console.log(n))
})();
