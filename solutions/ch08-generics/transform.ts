
function transform<T>(arr: T[], f: (k: T) => T): T[] {
    const result: T[] = [];
    for (let k = 0; k < arr.length; ++k) result.push(f(arr[k]));
    return result;
}

const N:number = 5;
const input = Array(N).fill(0).map((_, ix) => ix+1);
const result = transform(input, x => x * x);
console.log('%s --> %s', input, result);

const input2 = ['one', 'two', 'three'];
const result2 = transform(input2, k => k.toUpperCase());
console.log('%s --> %s', input2, result2);

const input3 = [false, true, false, true, false, true];
const result3 = transform(input3, x => !x);
console.log('%s --> %s', input3, result3);


//transform([10,20], k => k.toUpperCase()); //Property 'toUpperCase' does not exist on type 'number'.ts(2339)
//transform([10,'20'], k => k+42);          //Operator '+' cannot be applied to types 'string | number' and 'number'.ts(2365)
