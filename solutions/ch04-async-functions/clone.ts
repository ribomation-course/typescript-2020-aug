import * as FS from 'fs';
import * as UTIL from 'util';

const open  = UTIL.promisify(FS.open);
const fstat = UTIL.promisify(FS.fstat);
const read  = UTIL.promisify(FS.read);
const write = UTIL.promisify(FS.write);
const close = UTIL.promisify(FS.close);


async function copy(from: string, to: string) {
    try {
        let fd   = await open(from, 'r');
        let info = await fstat(fd);
        let buf  = Buffer.alloc(info.size);
        let data = await read(fd, buf, 0, buf.length, null);
        let txt  = data.buffer.toString();
        await close(fd);
        console.log('read   ', data.bytesRead, 'bytes from', from);
        
        let fd2   = await open(to, 'w');
        let data2 = await write(fd2, txt.toUpperCase());
        await close(fd2);
        console.log('written', data2.bytesWritten, 'bytes to  ', to);
    } catch (e) {
        console.log('*** Failed errcode=%s, syscall=%s, path=%s', e.code, e.syscall, e.path);
    }
}


const filename = './clone.ts';
copy(filename, filename + '.cpy');
//copy('whatever.txt', filename + '.cpy');
