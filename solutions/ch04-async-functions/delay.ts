export function delay(numSecs: number): Promise<any> {
    return new Promise((ok: any, err: any) => {
        setTimeout(() => {
            console.info('timeout after %d secs', numSecs);
            ok(numSecs);
        }, numSecs * 1000);
    });
}

function usingPromise() {
    console.info('[promise] delay...');
    delay(1)
        .then(n => {
            console.info('delay...OK', n, ' secs');
            return delay(4 * n);
        })
        .then(n => {
            console.info('delay...OK', n, ' secs');
        })
        ;
}

async function usingAsync() {
    console.info('[async] delay...');
    let n1 = await delay(1);
    let n2 = await delay(2 * n1);
    let n3 = await delay(2 * n2);
    console.info('---------');
    usingPromise();
}

usingAsync();
