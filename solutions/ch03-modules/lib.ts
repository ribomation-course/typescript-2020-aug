export function sum(n: number): number {
    if (n <= 0)  return 0;
    return n * (n + 1) / 2;
}

export function prod(n: number): number {
    if (n <= 0)  return 0;
    if (n === 1) return 1;
    return n * prod(n - 1);
}
