import * as process from 'process';
import {readFileSync} from 'fs';

const filename:string = process.argv[2] || __filename;
const content:string = readFileSync(filename).toString();
const words:string[] = content.split(/[^a-z]+/i);
console.log('num words in %s = %d', filename, words.length);
