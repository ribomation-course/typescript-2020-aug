import * as process from 'process';
import {sum, prod as factorial} from './lib';

const n:number = +(process.argv[2]) || 10;
console.log('SUM(%d) = %d', n, sum(n));
console.log('FAC(%d) = %d', n, factorial(n));
