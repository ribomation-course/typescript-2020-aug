
export default class Account {
    public static rate:number = 1.5;

    constructor(
        private _accno: string,
        private _balance: number
    ) {}

    toString():string {
        return `Account[${this._accno}, ${this._balance} kr]`;
    }

    get accno(): string {
        return this._accno.toUpperCase();
    }

    get balance():number {
        return this._balance;
    }

    set balance(amount: number) {
        if (amount <= 0) {
            throw new Error('amount cannot be negative')
        }
        this._balance = amount;
    }

    futureBalance(): number {
        return this._balance * (1 + Account.rate / 100)
    }

}

