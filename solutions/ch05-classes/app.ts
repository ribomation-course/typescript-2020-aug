
import Account from './account'

const a1 = new Account('seb123', 150)
console.log(a1.toString())

console.log('accno: %s', a1.accno)
console.log('balance: %d kr', a1.balance)

//a1.accno = 'xxxx' //Cannot assign to 'accno' because it is a read-only property.ts(2540)
//a1._accno = 'xxxx' //Property '_accno' is private and only accessible within class 'Account'.ts(2341)

a1.balance = a1.balance + 200
console.log('balance: %d kr', a1.balance)
console.log('futureBalance: %d kr', a1.futureBalance())

Account.rate *= 10
console.log('futureBalance: %d kr', a1.futureBalance())

const randBalance = () => 50 + 100*Math.random();
const accounts = ['seb123', 'hb456', 'sb678', 'nb987']
      .map(accno => new Account(accno, randBalance()));

console.log('-------')
accounts.forEach(a => console.log(a.toString()))
console.log('-------')
accounts.forEach(a => a.balance = a.futureBalance())
accounts.forEach(a => console.log(a.toString()))
