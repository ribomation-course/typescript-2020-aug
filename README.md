# TypeScript, 2 days
### 27 - 28 August 2020


Welcome to this course.
The syllabus can be found at
[javascript/typescript](https://www.ribomation.se/courses/javascript/typescript)

Here you can find
* [Installation instructions](./installation-instructions.md)
* [Solutions to the exercises](./solutions)


Course GIT Repo
====
It's recommended that you keep the git repo and your solutions separated. 
Create a dedicated directory for this course and a sub-directory for 
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/typescript-course/my-solutions
    cd ~/typescript-course
    git clone <git HTTPS clone link> gitlab

Get any updates by a `git pull` operation

    cd ~/typescript-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

